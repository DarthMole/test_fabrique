import requests
from random import randint


TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTU0MzAwMzYsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9kYXJ0aG1vbGUifQ.i6e4FE-9ZdvX5OtOV9TzAUZMIH_Gmozn2xvkWTZi_NA"
URL = "https://probe.fbrq.cloud/v1/send/{msg_id}"
headers = {"Authorization": f"Bearer {TOKEN}"}


def send_message(msg_id, phone, text):
    url = URL.format(msg_id=msg_id)
    data = {
        'id': msg_id,
        'phone': phone,
        'text': text
    }
    response = requests.post(url, json=data, headers=headers)
    if response.status_code == 200:
        return response.json()
    return {'code': response.status_code, 'message': response.reason}


if __name__ == '__main__':
    exit()
    for i in range(10000):
        msg_id = i + 1
        phone = randint(79000000000, 79999999999)
        text = "Hello World"

        result = send_message(msg_id, phone, text)

        if result.get('code') != 0:
            print({
                'id': msg_id,
                'phone': phone,
                'text': text
            })
            print(result)
